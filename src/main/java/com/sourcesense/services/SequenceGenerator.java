package com.sourcesense.services;

import com.sourcesense.dao.RandomNumberDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class SequenceGenerator {

    private static final int RANGE_START = 1;

    @Autowired
    private RandomNumberDao randomNumberDao;

    public List<String> generateSequenceForFizzBuzz(int rangeEnd) {
        return generateNumberSequence(rangeEnd).mapToObj(
                this::mapToStringAccordingToRule
        ).collect(Collectors.toList());
    }

    public List<String> generateSequenceForRsndomFizzBuzz() {
        return generateSequenceForFizzBuzz(randomNumberDao.retrieveRandomNumber());
    }

    private IntStream generateNumberSequence(Integer rangeEnd) {
        return IntStream.rangeClosed(RANGE_START, rangeEnd);
    }

    private String mapToStringAccordingToRule(int number) {
        String element = String.valueOf(number);
        if (number % 3 == 0 && number % 5 == 0) element = "FizzBuzz";
        else if (number % 3 == 0) element = "Fizz";
        else if (number % 5 == 0) element = "Buzz";
        return element;
    }

}
