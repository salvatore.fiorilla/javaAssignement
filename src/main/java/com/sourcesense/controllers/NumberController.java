package com.sourcesense.controllers;

import com.sourcesense.beans.Output;
import com.sourcesense.exceptions.FizzBuzzException;
import com.sourcesense.services.SequenceGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
public class NumberController {

    private static final Logger log = LoggerFactory.getLogger(NumberController.class);

    @Autowired
    private SequenceGenerator sequenceGenerator;

    @RequestMapping(value = "/fizzBuzz/{number}", method = RequestMethod.GET)
    public Output playFizzBuzz(@PathVariable("number") int number) throws Exception {
        log.info("Method playFizzBuzz - called with parameter " + number);
        if (number > 1000) throw new FizzBuzzException("Chosen number is larger than 1000.");
        List<String> sequence = sequenceGenerator.generateSequenceForFizzBuzz(number);
        log.info("Method playFizzBuzz - sequence generated");
        return new Output(sequence);
    }

    @RequestMapping(value = "/randomFizzBuzz", method = RequestMethod.GET)
    public Output playRandomFizzBuzz() throws Exception {
        log.info("Method playRandomFizzBuzz - called");
        List<String> sequence = sequenceGenerator.generateSequenceForRsndomFizzBuzz();
        log.info("Method playRandomFizzBuzz - sequence generated");
        return new Output(sequence);
    }

    @ExceptionHandler(FizzBuzzException.class)
    void handleBadRequests(HttpServletResponse response, FizzBuzzException ex) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
    }

    @ExceptionHandler(RestClientException.class)
    void handleTimeoutRequests(HttpServletResponse response, RestClientException ex) throws IOException {
        response.sendError(HttpStatus.GATEWAY_TIMEOUT.value(), "Not able to retrieve random number: connection timed out.");
    }

}
