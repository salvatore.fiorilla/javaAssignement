package com.sourcesense.beans;

import java.util.List;

public class Output {

    private List<String> outputList;

    public Output(List<String> outputList) {
        this.outputList = outputList;
    }

    public List<String> getOutputList() {
        return outputList;
    }

}
