package com.sourcesense.dao;

import com.sourcesense.beans.NumberFromApi;
import com.sourcesense.exceptions.FizzBuzzException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;


@Repository
public class RandomNumberDao {

    private static final Logger log = LoggerFactory.getLogger(RandomNumberDao.class);
    private static final int UPPER_LIMIT = 1000;

    private final RestTemplate restTemplate = new RestTemplate();

    public Integer retrieveRandomNumber() {
        log.debug("Method retrieveRandomNumber called");
        Integer number;
        try {
            NumberFromApi numberFromApi = restTemplate.getForObject("http://numbersapi.com/random/trivia?json", NumberFromApi.class);
            number = Integer.parseInt(numberFromApi.getNumber());
            log.debug("Method retrieveRandomNumber - number retrieved " + number);
            if (number > UPPER_LIMIT) throw new FizzBuzzException("Number exceeding the limit");
        } catch (NumberFormatException | FizzBuzzException ex) {
            log.debug("Method retrieveRandomNumber - Exception: " + ex.getMessage());
            number = retrieveRandomNumber();
            log.debug("Method retrieveRandomNumber - retrieved new number " + number);
        }
        log.debug("Method retrieveRandomNumber exiting");
        return number;
    }

}
