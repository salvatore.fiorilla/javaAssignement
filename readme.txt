Java Challenge Application
Web service exposing two endpoint:
- GET /fizzBuzz/{number} , returning a json array containing the number sequence from 1
    to the number specified in the call, where the number multiple of 3 are replaced with "Fizz",
    the multiple of 5 are replaced with "Buzz" and the multiple of both 3 and 5 with "FizzBuzz".
- GET /randomFizzBuzz , returning a json array as described before, but in this case the number is
    retrieved randomly from http://numbersapi.com/random/trivia?json.

The main class is in JavaChallengeApplication.java.
The two endpoints are defined in NumberController.java.
The service generating the output sequence is in SequenceGenerator.java.
The DAO retrieving random numbers from numbersapi is defined in RandomNumberDao.java.

To prevent the service from returning a too large sequence of numbers, the constraint of 1000 is set on the
largest accepted input number.

BUILD
No particular procedure is required to build and run the application in your IDE.
In order to create a standalone jar, run "mvn clean package". Maven will create the jar in the folder target.
To run the jar, execute java -jar javaChallengeApplication-1.0-SNAPSHOT.jar in terminal.
Once web service is up, it can be reached at localhost port 8080.
The port is specified in file resources/application.properties. You can specify a different port and rebuild
the application.

